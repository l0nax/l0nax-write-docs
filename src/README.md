# Documentation Writing Guide

Hier ein paar "Guide Lines" zum schreiben einer Dokumentation:
- Diese "Guide Line" **muss** gelesen werden  **bevor** eine Dokumenation geschrieben wird: [Why Writing Software Design Documents Matters](https://www.toptal.com/freelance/why-design-documents-matter)
- [How to document a Software Development Project](https://www.smartics.eu/confluence/display/PDAC1/How+to+document+a+Software+Development+Project)
- [10 Examples of Great End User Documentation](http://blog.screensteps.com/10-examples-of-great-end-user-documentation)
- [A beginner’s guide to writing documentation](http://docs.writethedocs.org/guide/writing/beginners-guide-to-docs/)
- [RTFM? How to write a manual worth reading](https://opensource.com/business/15/5/write-better-docs)

:warning: **Für das schreiben der API können folgende Wege eingegangen werden:**<br>
- Schreiben der API Dokumentation in Markdown (also GitBook) unter einsatz der folgenden Plugins: `api`, `hints`, `advanced-emoji` und `theme-api`
- Schreiben der API Dokumentation in API Blueprint (verwendung externer Software)

Sollte man sich für die GitBook Plugins entscheiden, bitte folgendes Lesen (und nicht die API Dokumentation den die ist für 'API Blueprint' gedacht!):<br>
## (gitbook-plugin-)api
Github Project des Plugins: [https://github.com/MagLoft/gitbook-plugin-api](https://github.com/MagLoft/gitbook-plugin-api)<br><br>
**Beispiel:**
:warning: **Hier kann leider kein Code Beispiel dieses Plugins gezeigt werden! Bitte die README des Projektes lesen!**
Compiled:<br>
{% api "List App Pages", method="GET", url="https://www.magloft.com/api/portal/v1/app_pages/:app_id/page/:page" %}

This endpoint **returns** a list of all `app pages` that belong to the magazine

### Parameters:

| Name       | Type    | Desc                                                |
| :--------- | :------ | :-------------------------------------------------- |
| **app_id** | String  | App ID to list app pages for                        |
| **page**   | Integer | The page to list                                    |
| per_page   | Integer | Number of items to show per page                    |
| order_by   | Symbol  | Field to sort results by                            |
| order_dir  | Symbol  | Direction (asc, desc) to sort results by            |
| filter     | String  | Text filter to search pages by name, title and html |

### Response:

```json
{
  "id": 1234,
  "title": "Welcome to MagLoft"
}
```

{% endapi %}

## (gitbook-plugin-)hints
Github Project des Plugins: [https://github.com/GitbookIO/plugin-hints](https://github.com/GitbookIO/plugin-hints)<br><br>
**Beispiel:**<br>
Code:<br>
```
{% hint style='info' %}
Important info: this note needs to be highlighted
{% endhint %}
```
Compiled:
- Style 'info'
{% hint style='info' %}
Important info: this note needs to be highlighted
{% endhint %}
- Style 'tip'
{% hint style='tip' %}
Important tip: this note needs to be highlighted
{% endhint %}
- Style 'danger'
{% hint style='danger' %}
Danger: this note needs to be highlighted
{% endhint %}
- Style 'working'
{% hint style='working' %}
Working: this note needs to be highlighted
{% endhint %}

Available styles are:
- info (default)
- tip
- danger
- working

**Erweiterte Configuration:**<br>
Die CSS Klassen können in der `book.json` Datei angepasst werden. Hier eine Beispiel `book.json` Datei:
```json
{
    "plugins": ["hints"],
    "pluginsConfig": {
        "hints": {
            "info": "fa fa-info-circle",
            "tip": "fa fa-mortar-board",
            "danger": "fa fa-exclamation-cicle",
            "working": "fa fa-wrench"
        }
    }
}
```

## (gitbook-plugin-)theme-api
Github Project des Themes: [https://github.com/GitbookIO/theme-api](https://github.com/GitbookIO/theme-api)<br><br>
Mit diesem Theme **kann** man an der Seite des Dokumentations Fensters eine extra Spalte für Beispiele der API hinzufügen.
**Bitte hierfür die [README](https://github.com/GitbookIO/theme-api/blob/master/README.md) des Projektes lesen!**<br>
[Hier](theme-api_example.md) befindet sich (kompiliertes) Beispiel.<br><br>

**Weitere gute GitBook Plugins:**
- [gitbook-plugin-custom-favicon](https://github.com/dtolb/gitbook-plugin-custom-favicon) Zum ändern des Favicons der Dokumentation.
- [gitbook-plugin-sectionx](https://github.com/ymcatar/gitbook-plugin-sectionx) [Click for Demo](https://ymcatar.gitbooks.io/gitbook-test/content/testing_sectionx.html)
- [gitbook-plugin-ace](https://github.com/ymcatar/gitbook-plugin-ace) [Click for Demo](https://ymcatar.gitbooks.io/gitbook-test/content/testing_ace.html)


**Weitere gute GitBook Themes:**
- [GitBook FAQ Theme](https://plugins.gitbook.com/plugin/theme-faq) Zum erstellen einer FAQ Seite. :wichtig: **Nicht getestete kompatibilität mit den Plugins `api`, `hints` & `advanced-emoji`**

<br>
<br>
<hr>
_Doc Infos:_
- Short Commit Hash: {{short_commit}}
