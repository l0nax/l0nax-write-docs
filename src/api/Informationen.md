# Informationen zu Api Blueprint

[Full Tutorial (english)](https://help.apiary.io/api_101/api_blueprint_tutorial/#metadata-api-name--description)

## Getting Started

Beispiel `index.apib`:
```
FORMAT: 1A

# The Simplest API
This is one of the simplest APIs written in the **API Blueprint**. One plain
resource combined with a method and that's it! We will explain what is going on
in the next installment -
[Resource and Actions](02.%20Resource%20and%20Actions.md).

**Note:** As we progress through the examples, do not also forget to view the
[Raw](https://raw.github.com/apiaryio/api-blueprint/master/examples/01.%20Simplest%20API.md)
code to see what is really going on in the API Blueprint, as opposed to just
seeing the output of the Github Markdown parser.

# Group Questions
Resources related to questions in the API.

## Question Collection [/questions]


Also please keep in mind that every single example in this course is a **real
API Blueprint** and as such you can **parse** it with the
[API Blueprint parser](https://github.com/apiaryio/drafter) or one of its
[bindings](https://github.com/apiaryio/drafter#bindings).


# GET /message
+ Response 200 (text/plain)

        Hello World!

```
### Begriffserklärung

#### Metadata, API Name & Beschreibung
Der erste Schritt um eine [API Blueprint][] Dokumentation zu schreiben ist API Name und Metadaten festzulegen:
```
FORMAT: 1A

# Polls

Polls is a simple API allowing consumers to view polls and vote in them.
```
:warning: Das **FORMAT** Keyword wird **benötigt**, es bezeichnet das Dokument als API Blueprint Dokument!<br>
Das Keyword **#** (im Beispiel **# Polls**) bezeichnet den API Namen.<br>

#### Ressourcen
API besteht aus Ressourcen, die durch ihre URIs spezifiziert sind. In diesem Beispiel haben wir eine Ressource namens `Question Collection`, mit der Sie eine Liste von Fragen einsehen können. Die Überschrift gibt ihre URI innerhalb von eckigen Klammern `[/questions]` an:
```
## Question Collection [/questions]
```

#### Aktionen \("Actions"\)
Sie sollten jede Aktion angeben, die Sie auf einer Ressource durchführen können. Eine Aktion wird mit einer Unterüberschrift mit dem Namen der Aktion gefolgt von der HTTP-Methode angegeben:
```
## Question Collection [/questions]

### List All Questions [GET]
An action should include at least one response from the server
which must include a status code and may contain a body.
A response is defined as a list item within an action.
Lists are created by preceding list items with either a +, * or -.

This action returns a 200 status code along with a JSON body.

+ Response 200 (application/json)

        {
            "question": "Favourite programming language?",
            "published_at": "2014-11-11T08:40:51.620Z",
            "url": "/questions/1",
            "choices": [
                {
                    "choice": "Swift",
                    "url": "/questions/1/choices/1",
                    "votes": 2048
                }, {
                    "choice": "Python",
                    "url": "/questions/1/choices/2",
                    "votes": 1024
                }, {
                    "choice": "Objective-C",
                    "url": "/questions/1/choices/3",
                    "votes": 512
                }, {
                    "choice": "Ruby",
                    "url": "/questions/1/choices/4",
                    "votes": 256
                }
            ]
        }
```

<hr>
[Get the full Example File in RAW Format](01_informationen-full_example.apib)
