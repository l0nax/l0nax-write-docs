# API Dokumentation

## Software

Zum erstellen der API Dokumentation wird die folgende Softwares verwendet:

* [API Blueprint][]:  "A powerful high-level API description language"
* [snowboard](https://github.com/bukalapak/snowboard):     '[API Blueprint][]' HTML Renderer

## Guide Aufbau

Diese Guide ist folgendermaßen aufgebaut:

1. Informationen zu [API Blueprint][]
2. Nützliches Wissen
3. Beispiele
4. CI Integration \(Documentation Building\)



<!-- LINKS -->
[API Blueprint]: https://apiblueprint.org/

