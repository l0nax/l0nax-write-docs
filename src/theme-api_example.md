{% method %}
## Install {#install}

The first thing is to get the GitBook API client.

{% sample lang="js" %}
```bash
$ npm install gitbook-api
```

{% sample lang="go" %}
```bash
$ go get github.com/GitbookIO/go-gitbook-api
```
{% endmethod %}

**Wichtig:** Die Sprachen müssen in der `book.json` angepasst werden! Hier ein Beispiel:
```json
{
  ...
  "pluginsConfig": {
  "theme-api": {
    "languages": [
    {
      "lang": "js",          // language
      "name": "JavaScript",  // corresponding name to be displayed
      "default": true        // default language to show
    },
    {
      "lang": "go",
      "name": "Go"
    }
      ]
    }
  }
}
```
