# CI/ CD Integration
Eine Erklärung zur automatisierung der Dokuementation eines Projektes.<br>
:warning: **Wichtig:** Im automatisierungs Projekt 'dashboard' von 'l0nax' wird die Dokumentation auf dem Server hochgeladen, **muss** aber kompiliert hochgeladen werden (HTML Dateien).
<br>

## CI/CD Software
Ich persönlich verwende am liebsten [Gitlab CI/CD](https://about.gitlab.com/features/gitlab-ci-cd/), die `.gitlab-ci.yml` ist einfach zu schreiben und zu handhaben.<br>
Sollten Gitlab Pages verwendet werden ist hier ein Auschnitt einer `.gitlab-ci.yml`:
```yaml
pages:
  image: debian:jessie
  stage: deploy
  script:
    - apt-get update && apt-get install curl sudo -y
    - curl -sL https://deb.nodesource.com/setup_8.x | bash -
    - apt-get install -y nodejs
    - npm install -g gitbook-cli
    - gitbook install
    - gitbook build
    - mkdir .public
    - cp -r _book/* .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - master
```
<br>

Sollte man andere Build Server Systeme einsetzten o. ä. dann hier ein Ablaufplan für die verwendung der Tools:
1. Installieren von `cURL` sollte man das automatisierungs Projekt 'dashboard' von 'l0nax' einsetzten.
2. Installation von [npm][] bzw. [NodeJs][].
3. [Installieren von](https://toolchain.gitbook.com/setup.html) `gitbook-cli` via [npm].
4. An alle `.md` Dateien 19 mal `<br>` anhängen (verbessert die Lesbarkeit der Dokumente) \[wird in der [api_blueprint.sh](#shell-file-for-find) gemacht\] **(optional)**
5. Sollte die API Dokumentation in [API Blueprint][] geschrieben sein (diese Dateien sollte die Dateiendung `.apib` haben)
  1. Installieren von [snowboard](https://github.com/bukalapak/snowboard) (siehe [README](https://github.com/bukalapak/snowboard))
  2. Die Datei `api_blueprint.sh` erstellen mit folgendem inhalt: [Click me](#shell-file-for-find)
  3. Datei ausführbar machen via `chmod +x api_blueprint.sh` (Unter Linux)
  3. Folgenden Befehl ausführen: `find . -name *.apib -type f -exec ./$(pwd)/api_blueprint.sh {} \;`
6. GitBook plugins installieren via `gitbook install`
7. Dokumentation generieren mit `gitbook build`
8. Die Dokumentation befindet sich nun im `_book/` Ordner.
9. **(optional)** Sollte man den Text `Published with GitBook` nicht in seiner Dokumenation haben wollen folgende befehle in seiner CI Build Configuration hinzufügen:
  1. Erstellen der [replace.sh](#shell-file-for-replace) Datei
  2. Datei ausführbar machen via `chmod +x replace.sh` (Unter Linux)
  3. Datei Ausführen via `./replace.sh` (unter Linux)

### Shell File for replace
Content of `replace.sh` File:
```shell
#!/bin/bash
find . -type f -print0 | xargs -0 sed -i 's/https:\/\/www.gitbook.com//g'
find . -type f -print0 | xargs -0 sed -i 's/<a href="" target="blank" class="gitbook-link">//g'
find . -type f -print0 | xargs -0 sed -i 's/Published with GitBook//g'
```

### Shell File for find
Content of `api_blueprint.sh` File:
```shell
#!/bin/bash
oldFileName="$1"
newFileName=$(echo $oldFileName | sed -e 's/.apib/.html/g' | sed -e ':a;N;$!ba;s/\n/ /g')

## append 19 * '<br>'
echo "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>" >> $oldFileName

snowboard html -o $newFileName $oldFileName

## delete '.apib' File
rm $oldFileName
```

<!-- LINKS -->
[npm]: https://nodejs.org/en/download/package-manager/
[NodeJs]: https://nodejs.org/en/
[API Blueprint]: https://apiblueprint.org/
